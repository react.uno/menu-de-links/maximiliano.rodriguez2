import React, {Fragment, useState, useEffect} from 'react';
import Header from './Components/Header';
import Botonera from './Components/Botonera';
import Spinner from './Components/Spinner';

function App() {

  const titulo='Tenes todas estas formas de contactarnos...';

  useEffect(()=>{
    consultarAPI();
  },[]);
 
const [linkList, guardarLinkList] = useState({});

const consultarAPI = async ()=>{
  const api= await fetch('http://phosphorus.alwaysdata.net/v1/users/6004940f8812576fb0a3508d');
  const linkList = await api.json();
  guardarLinkList(linkList); 
  console.log(linkList); 
}

  return (
<Fragment>
<Spinner/>
 {linkList.links ? <>
  <div className="container">
  <div className="row">
  <Header
  titulo={titulo}/>
  </div>
  <div>
  <Botonera
  name={linkList.name}
  links={linkList.links}
  />
  </div>
  
  </div>

  </> : null}
 
  </Fragment>
  );
}

export default App;
