import React, {useState} from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { withTheme } from '@emotion/react';


const ContenedorBotonera = styled.div`
    padding: 1rem;
    text-align: center;
    background-color: black;
    color: #FFF;
    margin-top: 1rem;
`;
const TextH2 = styled.h2`
text-align: center;
font-family: 'Slabo 27px', arial;
font-size: 2rem;
margin:0;
`;

const contenedorBtn = styled.div`
text-align: center;
font-family: 'Slabo 27px', arial;
font-size: 2rem;
margin:0;
`;
const Boton=styled.button`
background-color: grey;
font-size: 16px;
width:100%;
padding: 1rem;
color:#ffffff;
text-transform: uppercase;
font-weight: bold;
border:none;
transition: background-color .3s ease;
margin-top: 1rem;
&:hover{
    cursor:pointer;
    background-color: #26c6da;

}
`;

const Botonera = (props) => {

   
    return (
        <ContenedorBotonera>
            <TextH2>{props.name}</TextH2>
          
           <Boton href={props.links.Web}>Web</Boton>
           <Boton href={props.links.Facebook}>Facebook</Boton>
           <Boton href={props.links.Instagram}>Instagram</Boton>
           <Boton href={props.links.Github}>Github</Boton>

        </ContenedorBotonera>
     );
}


 
export default Botonera;