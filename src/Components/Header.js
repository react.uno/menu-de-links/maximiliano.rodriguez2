import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';

const ContenedorHeader = styled.header`
background-color: black;
padding: 10px;
font-weight: bold;
color: #26c6da;
`;

const TextH1 = styled.h1`
text-align: center;
font-family: 'Slabo 27px', arial;
font-size: 2rem;
margin:0;
`;


const Header = ({titulo}) => {
    return ( 
        <ContenedorHeader><TextH1>{titulo}</TextH1></ContenedorHeader>
     );
}
 
Header.propTypes ={
titulo: PropTypes.string.isRequired
}
export default Header;